<?php

namespace App\Http\Controllers;

use App\Models\Stipend;
use Illuminate\Http\Request;

class StipendController extends Controller {
	/**
	 * @var mixed
	 */
	protected $stipend;
	/**
	 * @param Stipend $stipend
	 */
	public function __construct(Stipend $stipend) {
		$this->stipend = $stipend;
	}

	public function index() {
		return $this->stipend->all();
	}
	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function store(Request $request) {
		return $this->stipend->firstOrCreate(['title' => $request->title], $request->all());
	}

	public function update(Request $request) {
		$find = $this->stipend->find($request->id);
		$response = $find->update($request->all());
		return $response === true ? $request->all() : [];
	}

	public function delete(int $id) {
		$response = $this->stipend->find($id)->delete();
		return response()->json($response);
	}
}
