<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $user = (Auth::check()) ? Auth::user()->name : null;
        return view('dashboard.home'); //, compact('user'));
    }

    /**
     * @param Request $request
     */
    public function logout(Request $request)
    {
        Auth::logout();
        Session::flush();
        return redirect('/home');
    }
}
