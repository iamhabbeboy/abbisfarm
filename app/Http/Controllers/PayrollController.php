<?php

namespace App\Http\Controllers;

use App\Models\Payroll;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    /**
     * @var mixed
     */
    protected $payroll;
    /**
     * @param Payroll $payroll
     */
    public function __construct(Payroll $payroll)
    {
        $this->payroll = $payroll;
    }

    public function index()
    {
        return $this->payroll->all();
    }

    public function store(Request $request)
    {
        return $this->payroll->firstOrCreate(['payday_date' => $request->payday_date], $request->all());
    }

    public function delete(int $id)
    {
        $response = $this->payroll->find($id)->delete();
        return response()->json($response);
    }

    public function all()
    {
        
    }
}
