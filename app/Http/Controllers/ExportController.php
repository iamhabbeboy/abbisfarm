<?php

namespace App\Http\Controllers;
use DB;

class ExportController extends Controller {

	/**
	 * @param int $id
	 * @return mixed
	 */
	public function index(int $id) {
		$query = DB::table('payslip')
			->where('payslip.payroll_id', '=', $id)
			->join('staffs', 'staffs.id', '=', 'payslip.user_id')
			->select('staffs.title', 'staffs.surname', 'staffs.othername', 'staffs.position', 'payslip.*')
			->get()->toArray();
		return view('dashboard.exportable', compact('query'));
	}
}
