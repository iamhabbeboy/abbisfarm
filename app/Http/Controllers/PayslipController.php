<?php

namespace App\Http\Controllers;

use App\Models\Payslip;
use Illuminate\Http\Request;

class PayslipController extends Controller {
	/**
	 * @var mixed
	 */
	protected $payslip;
	/**
	 * @param Payslip $payslip
	 */
	public function __construct(Payslip $payslip) {
		$this->payslip = $payslip;
	}

	/**
	 * @param $payroll_id
	 */
	public function index($payroll_id) 
	{
		$payslip_query = $this->payslip->loadInfo($payroll_id, $user_ids = null)->get();
		return response()->json($payslip_query);
	}

	public function fetch() 
	{
		$response = $this->payslip->all();
		return response()->json($response);
	}
	/**
	 * @param Request $request
	 * @param Payslip $payslip
	 */
	public function store(Request $request, Payslip $payslip) {
		$payroll_id = $request->payslip_id;
		$staff_ids = $request->data;
		$store = [];
		$retrieve = [];
		if (is_array($staff_ids)) {
			foreach ($staff_ids as $key => $staff) {
				# code...
				$fetch = $payslip->firstOrCreate(['payroll_id' => $payroll_id, 'user_id' => $staff], [
					'payroll_id' => $payroll_id, 'user_id' => $staff]);

				$payslip_query = $payslip->loadInfo($payroll_id, (int) $staff)->first();
				array_push($retrieve, $payslip_query);
			}
		}
		return response()->json($retrieve);
	}
	/**
	 * @param int $payslip_id
	 */
	public function delete(int $payslip_id) {
		$response = $this->payslip->find($payslip_id)->delete();
		return response()->json($response);
	}
}
