<?php

namespace App\Http\Controllers;

use App\Models\Position;
use Illuminate\Http\Request;

class PositionController extends Controller
{
    /**
     * @var mixed
     */
    protected $position;
    /**
     * @param Position $position
     */
    public function __construct(Position $position)
    {
        $this->position = $position;
    }

    public function index()
    {
        return $this->position->all();
    }
    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        return $this->position->firstOrCreate(['title' => $request->title], $request->all());
    }
}
