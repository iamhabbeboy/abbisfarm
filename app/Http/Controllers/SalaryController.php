<?php

namespace App\Http\Controllers;

use App\Models\Salary;
use Illuminate\Http\Request;

class SalaryController extends Controller {
	/**
	 * @var mixed
	 */
	protected $salary;
	/**
	 * @param Salary $salary
	 */
	public function __construct(Salary $salary) {
		$this->salary = $salary;
	}

	/**
	 * @return mixed
	 */
	public function index() {
		return $this->salary->loadInfo()->get();
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'staff_id' => 'required|integer',
			'amount' => 'required',
		]);
		$request = $this->salary->firstOrCreate(['staff_id' => $request->staff_id], $request->all());
		$response = $this->salary->loadInfo($request->id)->first();
		return response()->json($response);
	}

	public function delete($salary_id = null) {
		$response = $this->salary->find($salary_id)->delete();
		return response()->json($response);
	}

	public function single(int $staff_id) {
		$response = $this->salary->where('staff_id', $staff_id)
			->get();
		return response()->json($response);
	}
}
