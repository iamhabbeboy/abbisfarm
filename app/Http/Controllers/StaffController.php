<?php

namespace App\Http\Controllers;

use App\Models\Staff;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StaffController extends Controller {
	/**
	 * @var mixed
	 */
	protected $staff;

	/**
	 * @param Staff $staff
	 */
	public function __construct(Staff $staff) {
		$this->staff = $staff;
	}

	/**
	 * @return mixed
	 */
	public function index() {
		$response = $this->staff->all();
		return response()->json($response);
	}

	/**
	 * @param Request $request
	 */
	public function store(Request $request) {
		$this->validate($request, [
			'title' => 'required',
			'surname' => 'required',
			'othername' => 'required',
			'phone_number' => 'required',
			'position' => 'required',
			'state' => 'required',
		]);

		$phone_number = $request->phone_number;
		$staff = $this->staff->firstOrCreate(['phone_number' => $phone_number], $request->all());

		$response = $staff;
		$response['status'] = 'success';

		return response($response->jsonSerialize(), Response::HTTP_CREATED);
	}

	/**
	 * @param Request $request
	 * @return mixed
	 */
	public function update(Request $request) {
		$find = $this->staff->find($request->id);
		$response = $find->update($request->all());
		return $response === true ? $request->all() : [];
	}
}
