<?php

namespace App\Http\Controllers;

use App\Models\Stipend;
use App\Models\Summary;
use Illuminate\Http\Request;

class SummaryController extends Controller {
	/**
	 * @var mixed
	 */
	protected $summary;
	/**
	 * @param Summary $summary
	 */
	public function __construct(Summary $summary) {
		$this->summary = $summary;
	}

	public function index() {
	}

	/**
	 * @param Request $request
	 * @param Stipend $stipend
	 */
	public function store(Request $request) {
		$response = $this->summary->firstOrCreate([
			'staff_id' => $request->staff_id,
			'stipend_id' => $request->stipend_id,
			'payslip_id' => $request->payslip_id,
		], $request->all());
		$stipends = $this->summary
			->loadSummary($request->staff_id, $request->stipend_id, $request->payslip_id);
		return response()->json($stipends->first());
	}

	/**
	 * @param int $staff_id
	 */
	public function single($_id) {
		$split_id = explode(":", $_id);
		$response = $this->summary->loadSummary((int) $split_id[0], null, (int) $split_id[1]);
		return response()->json($response->get());
	}

	/**
	 * @param int $id
	 * @return mixed
	 */
	public function delete(int $id) {
		$response = $this->summary->find($id)->delete();
		return response()->json($response);
	}
}
