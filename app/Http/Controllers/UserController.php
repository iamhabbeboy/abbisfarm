<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller {
	/**
	 * @var mixed
	 */
	protected $user;
	/**
	 * @param User $user
	 */
	public function __construct(User $user) {
		$this->user = $user;
	}

	public function index() {
		$users = $this->user->all();
		return response()->json($users);
	}

	/**
	 * @param Request $request
	 */
	public function store(Request $request) {
		$data = $request->all();
		$data['password'] = Hash::make($request->password);
		$user = $this->user->firstOrCreate(['email' => $request->email], $data);
		return response()->json($user);
	}

	public function update(Request $request) {
		$response = $this->user->find($request->id);
		if ($request->name) {
			$data = $request->all();
			$data['password'] = Hash::make($request->password);
			$response->fill($request->all());
		} else {
			$response->fill(['priority' => $request->priority]);
		}
		return response()->json($response);
	}

	public function delete(int $id) {
		$response = $this->user->find($id)->delete();
		return response()->json($response);
	}
}
