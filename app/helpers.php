<?php
use App\Models\Salary;
use App\Models\Stipend;
use App\Models\Summary;

function basic_salary($id) {
	$query = Salary::where('staff_id', $id);
	return $query->count() ? $query->first()->amount : 0;
}

function stipend($payslip_id, $staff_id, $type, $title = null) {
	$query = new Summary;
	$response = $query->loadSummary($staff_id, $stipend_id = null, $payslip_id);
	$toArray = $response->get()->toArray();
	return $title === 'amount' ? filter_amount($toArray, $type) : filter($toArray, $type);
}
function filter_amount($list, $token_type) {
	return array_map(function ($value) use ($token_type) {
		$token = array_get($value, 'has_stipend.token_type');
		return ($token === $token_type) ? (float) array_get($value, 'has_stipend.amount')
		: '';
	}, $list);
}
function filter($list, $token_type) {
	return array_map(function ($value) use ($token_type) {
		$token = array_get($value, 'has_stipend.token_type');
		return ($token === $token_type) ? array_get($value, 'has_stipend.title')
		: '';
	}, $list);
}
