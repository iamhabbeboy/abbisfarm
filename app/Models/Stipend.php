<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stipend extends Model
{
    /**
     * @var string
     */
    protected $table = 'stipend';
    /**
     * @var array
     */
    protected $fillable = ['title', 'token_type', 'amount'];
}
