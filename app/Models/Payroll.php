<?php

namespace App\Models;

use App\Models\Payslip;
use Illuminate\Database\Eloquent\Model;

class Payroll extends Model {
	/**
	 * @var string
	 */
	protected $table = 'payroll';
	/**
	 * @var array
	 */
	protected $fillable = ['month', 'year', 'payday_date'];

	public function hasPayslip() {
		return $this->belongsTo(Payslip::class, 'payroll_id', 'id');
	}

	public function hasSummary() {
		// return $this->
	}
}
