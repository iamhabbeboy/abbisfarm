<?php

namespace App\Models;

use App\Models\Payroll;
use App\Models\Staff;
use Illuminate\Database\Eloquent\Model;

class Payslip extends Model {
	/**
	 * @var string
	 */
	protected $table = 'payslip';
	/**
	 * @var array
	 */
	protected $fillable = ['payroll_id', 'user_id', 'extra_pay_id'];

	/**
	 * @return mixed
	 */
	public function hasPayroll() {
		return $this->belongsTo(Payroll::class, 'payroll_id', 'id');
	}

	/**
	 * @return mixed
	 */
	public function hasSummary() {
		return $this->hasMany(Summary::class, 'payslip_id');
	}

	public function loadSummary() {
		return $this->with('hasSummary');
	}

	/**
	 * @return mixed
	 */
	public function hasStaff() {
		return $this->belongsTo(Staff::class, 'user_id', 'id');
	}

	/**
	 * @param $id
	 * @param null $staff_ids
	 * @return mixed
	 */
	public function loadInfo($id = null, $staff_ids = null) {
		$model = null;
		if ($staff_ids === null) {
			$model = $this->where('payroll_id', $id)->with('hasStaff');
		} else {
			$model = $this->where('payroll_id', $id)->where('user_id', $staff_ids)->with('hasStaff');
		}
		return $model;
	}
}
