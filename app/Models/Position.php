<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    //
    /**
     * @var string
     */
    protected $table = 'position';
    /**
     * @var array
     */
    protected $fillable = ['title'];
}
