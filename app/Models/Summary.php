<?php

namespace App\Models;

use App\Models\Stipend;
use Illuminate\Database\Eloquent\Model;

class Summary extends Model {
	/**
	 * @var string
	 */
	protected $table = 'summary';
	/**
	 * @var array
	 */
	protected $fillable = ['staff_id', 'stipend_id', 'payslip_id'];

	public function hasStipend() {
		return $this->belongsTo(Stipend::class, 'stipend_id', 'id');
	}

	public function loadSummary(int $staff_id, $stipend_id = null, int $payslip_id) {
		$query = null;
		if ($stipend_id === null) {
			$query = $this->where('staff_id', $staff_id)
				->where('payslip_id', $payslip_id);
		} else {
			$query = $this->where('staff_id', $staff_id)
				->where('stipend_id', $stipend_id)
				->where('payslip_id', $payslip_id);
		}
		return $query->with('hasStipend');
	}
}
