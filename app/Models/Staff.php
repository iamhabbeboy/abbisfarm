<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staffs';
    protected $fillable = [
        'title',
        'surname',
        'othername',
        'email_address',
        'phone_number',
        'qualification',
        'date_of_birth',
        'position',
        'state',
        'lga',
        'home_address',

    ];
}
