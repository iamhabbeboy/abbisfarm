<?php

namespace App\Models;

use App\Models\Staff;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    /**
     * @var string
     */
    protected $table = 'salary';
    /**
     * @var array
     */
    protected $fillable = ['staff_id', 'amount'];

    public function hasStaff()
    {
        return $this->belongsTo(Staff::class, 'staff_id');
    }
    public function loadInfo($id = null)
    {
        $query = null;
        if ($id === null) {
            $query = $this->whereNotNull('staff_id');
        } else {
            $query = $this->where('id', $id);
        }
        return $query->with('hasStaff');
    }
}
