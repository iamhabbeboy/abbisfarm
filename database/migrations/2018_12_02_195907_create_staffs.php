<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('surname');
            $table->string('othername');
            $table->string('email_address')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('qualification')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('position')->nullable();
            $table->string('state')->nullable();
            $table->string('lga')->nullable();
            $table->string('home_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
