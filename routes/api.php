<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});
sleep(9);
Route::get('/staffs', 'StaffController@index');
Route::post('/staffs', 'StaffController@store');
Route::put('/staffs', 'StaffController@update');

Route::post('/position', 'PositionController@store');
Route::get('/position', 'PositionController@index');

Route::post('/stipend', 'StipendController@store');
Route::get('/stipend', 'StipendController@index');
Route::put('/stipend', 'StipendController@update');
Route::delete('/stipend/{id}', 'StipendController@delete');
//---------💰- PAYROLL---------------------------
Route::get('/payroll', 'PayrollController@index');
Route::post('/payroll', 'PayrollController@store');
Route::delete('/payroll/{id}', 'PayrollController@delete');
// Route::get('/payroll', 'PayrollController@all');
//--------------- 💵 PAYSLIP ------------
Route::post('/payslip', 'PayslipController@store');
Route::get('/payslip', 'PayslipController@fetch');
Route::get('/payslip/{payroll_id}', 'PayslipController@index');
Route::delete('/payslip/{payroll_id}', 'PayslipController@delete');
// -------------------- SALARY ---------------
Route::get('/salary', 'SalaryController@index');
Route::get('/salary/{staff_id}', 'SalaryController@single');
Route::post('/salary', 'SalaryController@store');
Route::delete('/salary/{salary_id}', 'SalaryController@delete');

// ----------------- SUMMARY ------------------------
Route::get('/summary', 'SummaryController@index');
Route::get('/summary/{staff_id}', 'SummaryController@single');
Route::post('/summary', 'SummaryController@store');
Route::delete('/summary/{id}', 'SummaryController@delete');

Route::get('/users', 'UserController@index');
Route::post('/users', 'UserController@store');
Route::put('/users', 'UserController@update');
Route::delete('/users/{id}', 'UserController@delete');
