<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
use Illuminate\Support\Facades\Auth;

Route::get('/home/{vue_capture?}', function () {
	$user = (Auth::check()) ? Auth::user() : null;
	return view('dashboard.home', compact('user'));
})->where('vue_capture', '[\/\w\.-]*')->middleware('auth');

Auth::routes();
Route::get('/export/{id}', 'ExportController@index');
Route::get('/signout', 'HomeController@logout');
