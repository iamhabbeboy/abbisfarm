@extends('layouts.app')
@section('content')
<style>
.font-increase {
	font-size: 14px;
	font-weight: bold;
	background-color: #999 !important;
	color: #DDD;
}
</style>
	<div class="row">
		{{-- {{dd($query)}} --}}
	<div class="container">
	<span style="float:right">
		<a href="javascript:window.print()">[click to print]</a>
	</span>
	<table class="table table-bordered table-striped" align="center" style="font-size: 14px;">
		<tr>
			<th colspan="7">
				<h2>Payroll </h2>
			</th>
		</tr>
		<tr class="font-increase">
			<th>S/N</th>
			<th>Name</th>
			<th>Position</th>
			<th>Basic Salary</th>
			<th> Bonus </th>
			<th> Deduction </th>
			<th>Total</th>
		</tr>
		@php
			$grand_total = 0;
			$bonus = 0;
			$deduct = 0;
			$total_salary = 0;
		@endphp
		@foreach($query as $key => $value)
			@php $total = 0 @endphp
		<tr>
			<th>{{$key+1}}</th>
			<th>{{$value->title}} {{$value->surname}} {{$value->othername}}</th>
			<th>{{$value->position}}</th>
			<th> @money(basic_salary($value->user_id), 'NGN')
				@php
				 $total = $total + (float) basic_salary($value->user_id);
				 $total_salary  = $total_salary + (float) basic_salary($value->user_id);
				@endphp
			</th>
			<th> <small>{{ count(stipend($value->id, $value->user_id, 'earning')) > 0 ? implode(', ', array_filter(stipend($value->id, $value->user_id, 'earning'))): '' }}</small>
				<br>
				<small>{{ count(stipend($value->id, $value->user_id, 'earning', 'amount')) > 0 ? implode(' + ', array_filter(stipend($value->id, $value->user_id, 'earning', 'amount'))): '' }}</small>
				<br>
				<b>Total: @money(array_sum(stipend($value->id, $value->user_id, 'earning', 'amount')), 'NGN') </b>
				@php
				$total = $total + (float) array_sum(stipend($value->id, $value->user_id, 'earning', 'amount'));
				$bonus = $bonus + (float) array_sum(stipend($value->id, $value->user_id, 'earning', 'amount'));
				@endphp
			 </th>
			<th> <small>{{ count(stipend($value->id, $value->user_id, 'deduction')) > 0 ? implode(',', array_filter(stipend($value->id, $value->user_id, 'deduction'))): '' }} </small>
				<br>
				<small>{{ count(stipend($value->id, $value->user_id, 'deduction', 'amount')) > 0 ? implode(' + ', array_filter(stipend($value->id, $value->user_id, 'deduction', 'amount'))): '' }}</small>
				<hr>
				<b>Total:  @money(array_sum(stipend($value->id, $value->user_id, 'deduction', 'amount')), 'NGN')</b>
				@php
					$total = $total - (float) array_sum(stipend($value->id, $value->user_id, 'deduction', 'amount'));
					$deduct = $deduct + (float) array_sum(stipend($value->id, $value->user_id, 'deduction', 'amount'));
				@endphp
			</th>
			<th>
				 @money($total, 'NGN')
				@php
				 	$grand_total = $grand_total + (float) $total
				@endphp
			</th>
		</tr>
		@endforeach
		<tr>
			<th>&nbsp;</th>
			<th colspan="2"><h4>Grand total</h4></th>
			<th> <h4>@money($total_salary, 'NGN') </h4></th>
			<th> <h4>@money($bonus, 'NGN')</h4></th>
			<th> <h4>@money($deduct, 'NGN')</h4></th>
			<th > <h4>@money($grand_total, 'NGN')</h4></th>
		</tr>
	</table>
</div>
	</div>
@stop