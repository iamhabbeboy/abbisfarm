@extends('layout.admin')
@section('content-body')
<div class="wrapper">

  <!--Navbar 🔥-->
  <nav-component></nav-component>
  {{-- {{dd($user)}} --}}
  <sidebar-component name="{{$user->name}}" priority="{{$user->priority}}"></sidebar-component>

  <div class="content-wrapper">

	<navbar-component></navbar-component>

  <section class="content">
	 <router-view></router-view>
	</section>

  </div>
  <!-- /.content-wrapper -->
<foot-component></foot-component>

</div>
<!-- ./wrapper -->

@stop