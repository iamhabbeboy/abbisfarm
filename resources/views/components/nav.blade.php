  <header class="main-header">
    <!-- Logo -->
    <a href="index.html" class="logo">
      <!-- mini logo -->
	  <b class="logo-mini">
		  <span class="light-logo"><img src="/dashboard/images/logo-light.png" alt="logo"></span>
		  <span class="dark-logo"><img src="/dashboard/images/logo-dark.png" alt="logo"></span>
	  </b>
      <!-- logo-->
      <span class="logo-lg">
		  <img src="/dashboard/images/logo-light-text.png" alt="logo" class="light-logo">
	  	  <img src="/dashboard/images/logo-dark-text.png" alt="logo" class="dark-logo">
	  </span>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
	  <div>
		  <a href="index.html#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		  </a>


	  </div>

      <div class="navbar-custom-menu">
      </div>
    </nav>
  </header>