import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import VueAxios from 'vue-axios';

import mutation from './store/mutation';
import action from './store/action';
import getters from './store/getters';

Vue.use(Vuex);
Vue.use(VueAxios, axios);

export default new Vuex.Store({
    state: {
        staffs: [],
        positions: [],
        stipends: [],
        payrolls: [],
        payslips: [],
        payslip_only: [],
        salarys: [],
        summarys: [],
        users: []
    },
    actions: action,
    mutations: mutation,
    getters: getters
})
