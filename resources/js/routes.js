import Home from './pages/Home';
import AddStaff from './pages/AddStaff';
import StaffDetails from './pages/StaffDetails';
import StaffSingleDetails from './pages/StaffSingleDetails';
import SalaryPlan from './pages/SalaryPlan';
import Payroll from './pages/Payroll';
import ManagePayroll from './pages/ManagePayroll';
import SinglePayroll from './pages/SinglePayroll';
import Settings from './pages/Settings';

const routes = [
	{ path: '/home', component: Home, name: 'Home' },
    { path: '/home/add-staff/:id?', component: AddStaff, name: 'AddStaff' },
    { path: '/home/staff-details', component: StaffDetails, name: 'StaffDetails' },
    { path: '/home/staff-details/:id', component: StaffSingleDetails, name: 'StaffSingleDetails' },
    { path: '/home/salary-plan', component: SalaryPlan, name: 'SalaryPlan'},
    { path: '/home/payroll', component: Payroll, name: 'Payroll'},
    { path: '/home/manage/:id', component: ManagePayroll, name: 'ManagePayroll'},
    { path: '/home/payroll/:id', component: SinglePayroll, name: 'SinglePayroll'},
    { path: '/home/setting/:id?', component: Settings, name: 'Settings'},
    { path: '*', redirect: '/home' },
];


export default routes;