import api from './api-route'

export default {
	save: (prefix, data) => {
		return axios.post(api.url[prefix].post, data);
	},
	get: (prefix, data) => {
		let query = '';
		if (Object.keys(data).length > 0) {
			let _id = data.id || 0;
			query = `${api.url[prefix].get}/${_id}`;
		} else {
			query = api.url[prefix].get;
		}
		return axios.get(query);
	},
	put: (prefix, data) => {
		return axios.put(api.url[prefix].put, data);
	},
	delete: (prefix, data) => {
		return axios.delete(`${api.url[prefix].delete}/${data.id}`);
	}
}