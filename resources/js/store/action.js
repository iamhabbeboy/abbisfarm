import api from '../api-route';
import http from '../http';

export default {
    async loadStaffs({ commit }) {
        try {
            const staffs = await http.get('staff', {});
            commit('SET_STAFFS', staffs.data);
        } catch (error) {
            commit('SET_STAFFS', []);
        }
    },
    async storeposition({ commit }, payload) {
        try {
            const position = await http.save('position', payload);
            commit('ADD_POSITION', position.data);
        } catch (error) {
            commit('ADD_POSITION', []);
        }
    },
    async loadPosition({ commit }) {
        try {
            const position = await http.get('position', {});
            commit('SET_POSITION', position.data);
        } catch (error) {
            commit('SET_POSITION', []);
        }
    },
    async stipendList({ commit }, payload) {
        try {
            const stipend = await http.save('stipend', payload);
            commit('ADD_STIPEND', stipend.data);
        } catch (error) {
            commit('ADD_STIPEND', []);
        }
    },
    async loadStipends({ commit }) {
        try {
            const stipend = await http.get('stipend', {});
            commit('SET_STIPEND', stipend.data);
        } catch (error) {
            commit('SET_STIPEND', []);
        }
    },
    async updatestipend({ commit }, payload) {
        try {
            const stipend = await http.put('stipend', payload);
            commit('UPDATE_STIPEND', stipend.data)
        } catch (error) {
            commit('UPDATE_STIPEND', []);
        }
    },
    async removestipend({ commit }, payload) {
        try {
            const stipend = await http.delete('stipend', payload);
            commit('DELETE_STIPEND', payload);
        } catch (error) {
            commit('DELETE_STIPEND', [])
        }
    },
    async updatestaff({ commit }, payload) {
        try {
            const staff = await http.put('staff', payload);
            commit('UPDATE_STAFF', staff.data)
        } catch (error) {
            commit('UPDATE_STAFF', []);
        }
    },
    async storepayroll({ commit }, payload) {
        try {
            const payroll = await http.save('payroll', payload);
            commit('ADD_PAYROLL', payroll.data);
        } catch (error) {
            commit('ADD_PAYROLL', []);
        }
    },
    async loadpayroll({ commit }) {
        try {
            const payroll = await http.get('payroll', {});
            commit('SET_PAYROLL', payroll.data);
        } catch (error) {
            commit('SET_PAYROLL', []);
        }
    },
    async removepayroll({ commit }, payload) {
        try {
            const payroll = await http.delete('payroll', payload);
            commit('DELETE_PAYROLL', payload);
        } catch (error) {
            commit('DELETE_PAYROLL', []);
        }
    },
    async storepayslip({ commit }, payload) {
        try {
            const payslip = await http.save('payslip', payload);
            commit('ADD_PAYSLIP', payslip.data);
        } catch (error) {
            commit('ADD_PAYSLIP', []);
        }
    },
    async loadpayslip({ commit }, payload) {
        try {
            const payslip = await http.get('payslip', payload);
            commit('SET_PAYSLIP', payslip.data);
        } catch (error) {
            commit('SET_PAYSLIP', []);
        }
    },
    async loadpaysliponly({ commit }) {
        try {
            const payslip = await http.get('payslip', {});
            commit('SET_PAYSLIP_ONLY', payslip.data);
        } catch (error) {
            commit('SET_PAYSLIP_ONLY', []);
        }
    },
    async removepayslip({ commit }, payload) {
        try {
            const payslip = await http.delete('payslip', payload);
            commit('REMOVE_PAYSLIP', payload);
        } catch (error) {
            commit('REMOVE_PAYSLIP', []);
        }
    },
    async loadSalary({ commit }) {
        try {
            const salary = await http.get('salary', {});
            commit('SET_SALARY', salary.data);
        } catch (error) {
            commit('SET_SALARY', []);
        }
    },
    async storesalary({ commit }, payload) {
        try {
            const salary = await http.save('salary', payload);
            commit('ADD_SALARY', salary.data);
        } catch (error) {
            commit('ADD_SALARY', []);
        }
    },
    async deletesalary({ commit }, payload) {
        try {
            const salary = await http.delete('salary', payload);
            commit('DELETE_SALARY', payload);
        } catch (error) {
            commit('DELETE_SALARY', []);
        }
    },
    async storesummary({ commit }, payload) {
        try {
            const summary = await http.save('summary', payload);
            commit('ADD_SUMMARY', summary.data);
        } catch (error) {
            commit('ADD_SUMMARY', []);
        }
    },
    async loadsummary({ commit }, payload) {
        try {
            const summary = await http.get('summary', payload);
            commit('SET_SUMMARY', summary.data);
        } catch (error) {
            commit('SET_SUMMARY', []);
        }
    },
    async removesummary({ commit }, payload) {
        try {
            const summary = await http.delete('summary', payload);
            commit('DELETE_SUMMARY', payload);
        } catch (error) {
            commit('DELETE_SUMMARY', []);
        }
    },
    async loadusers({ commit }) {
        try {
            const users = await http.get('users', {});
            commit('SET_USER', users.data);
        } catch (error) {
            commit('SET_USER', []);
        }
    },
    async storeuser({ commit }, payload) {
        try {
            const users = await http.save('users', payload);
            commit('ADD_USER', users.data);
        } catch (error) {
            commit('ADD_USER', []);
        }
    },
    async updateuser({ commit }, payload) {
        try {
            const users = await http.put('users', payload);
            commit('UPDATE_USER', users.data);
        } catch (error) {
            commit('UPDATE_USER', []);
        }
    },
    async removeuser({ commit }, payload) {
        try {
            // const users = await http.delete('users', payload);
            commit('DELETE_USER', payload);
        } catch (error) {
            commit('DELETE_USER', []);
        }
    }

}
