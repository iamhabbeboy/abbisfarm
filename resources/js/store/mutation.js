export default {
    SET_STAFFS(state, staffs) {
        state.staffs = staffs
    },
    SET_POSITION(state, positions) {
        state.positions = positions
    },
    ADD_POSITION(state, positions) {
        state.positions.push(positions)
    },
    SET_STIPEND(state, stipends) {
        state.stipends = stipends
    },
    ADD_STIPEND(state, stipends) {
        state.stipends.push(stipends);
    },
    UPDATE_STIPEND(state, stipends) {
        const resp = state.stipends.filter(r => {
            if (r.id === stipends.id) {
                r.title = stipends.title
                r.amount = stipends.amount
            }
        });
    },
    DELETE_STIPEND(state, payload) {
        const response = state.stipends.find(stipend => stipend.id === payload.id);
        state.stipends.splice(response, 1);
    },
    UPDATE_STAFF(state, staff) {
        const resp = state.staffs.filter(r => {
            if (r.id === staff.id) {
                r.title = staff.title
                r.surname = staff.surname,
                    r.date_of_birth = staff.date_of_birth,
                    r.email_address = staff.email_address,
                    r.lga = staff.lga,
                    r.othername = staff.othername,
                    r.position = staff.position
            }
        });
    },
    SET_PAYROLL(state, payroll) {
    	state.payrolls = payroll;
    },
    ADD_PAYROLL(state, payroll) {
    	state.payrolls.push(payroll);
    },
    DELETE_PAYROLL(state, payload) {
        const response = state.payrolls.find(payroll => payroll.id === payload.id);
        state.payrolls.splice(response, 1);
    },
    SET_PAYSLIP(state, payslip) {
        state.payslips = payslip;
    },
    SET_PAYSLIP_ONLY(state ,payslip) {
        state.payslip_only = payslip
    },
    ADD_PAYSLIP(state, payslip) {
        let list;
        for(list of payslip) {
            state.payslips.push(list);
        }
    },
    REMOVE_PAYSLIP(state, payload) {
        const response = state.payslips.find(payslip => payslip.id === payload.id);
        state.payslips.splice(response, 1);
    },
    SET_SALARY(state, salary) {
        state.salarys = salary;
    },
    ADD_SALARY(state, salary) {
        state.salarys.push(salary);
    },
    DELETE_SALARY(state, payload) {
        const response = state.salarys.find(salary => salary.id === payload.id);
        state.salarys.splice(response, 1);
    },
    ADD_SUMMARY(state, summary) {
        state.summarys.push(summary);
    },
    SET_SUMMARY(state, summary) {
        state.summarys = summary;
    },
    DELETE_SUMMARY(state, summary) {
        const response = state.summarys.find(summary => summary.id === summary.id);
        state.summarys.splice(response, 1);
    },
    SET_USER(state, user) {
        state.users = user;
    },
    ADD_USER(state, user) {
        state.users.push(user);
    },
    UPDATE_USER(state, user) {
         const resp = state.users.filter(r => {
            if (r.id === user.id) {
                r.priority = user.priority;
                if (user.name) {
                    r.name = user.name;
                    r.email = user.email;
                }
            }
        });
    },
     DELETE_USER(state, payload) {
        const response = state.users.find(user => user.id === payload.id);
        console.log(response)
        state.users.splice(response, 1);
    },
   
}
