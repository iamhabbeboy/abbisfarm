export default {
    findStaffById: (state, getters) => (id) => {
        return state.staffs.find(staff => staff.id === id)
    },
    findPayrollById: (state, getters) => (id) => {
        return state.payrolls.find(payroll => payroll.id === id)
    },
    findEarning: (state, getters) => (type) => {
        return state.stipends.filter(earning => earning.token_type === 'earning')
    },
    findDeduction: (state, getters) => (type) => {
        return state.stipends.filter(earning => earning.token_type === 'deduction');
    },
    findPayslipById: (state, getters) => (id) => {
        return state.payslips.find(payslip => payslip.has_staff.id === id);
    },
    findSalaryByStaff: (state, getters) => (staff_id) => {
        return state.salarys.find(salary => salary.staff_id === staff_id);
    },
    findStipendByStaff: (state, getters) => (type) => {
        return state.summarys.find(summary => summary.has_stipend.token_type === type);
    },
    totalStaffNo: (state, getters) => {
        return state.staffs.length;
    },
    totalPayroll: (state, getters) => {
        return state.payrolls.length;
    },
    totalPayslip: (state, getters) => {
        return state.payslip_only.length;
    },
    findPayrollByParam: (state, getters) => (l) => {
        const resp = state.payrolls.find(payroll => payroll.month === l.month && payroll.year === l.year);
        return resp;
    },
    findStaffByName: (state, getters) => (profile) => {
        return state.staffs.filter(staff => {
            if (staff.title === profile.title &&
                staff.surname.toLowerCase() === profile.surname.toLowerCase() &&
                staff.othername.toLowerCase().includes(profile.othername.toLowerCase())
                ) {
                return staff;
            }
        });
    },
    totalUser: (state, getters) => {
        return state.users.length;
    },
    findUserById: (state, getters) => (id) => {
        return state.users.find(user => user.id === parseInt(id));
    }
}
