import swal from 'sweetalert'

export default {
    error: (err) => {
        if (err.response.data.errors) {
            let errors = err.response.data.errors;
            let i, html_error = '';
            let objKeys = Object.keys(errors);
            let objVal = Object.values(errors);

            for (i = 0; i < objKeys.length; i++) {
                html_error += `<div><b style="color: #993300">${objKeys[i].toUpperCase()}:</b> ${objVal[i][0]}</div>`;
            }

            const divElement = document.createElement('div');
            divElement.innerHTML = html_error;

            swal({
                title: 'Notification',
                icon: 'warning',
                content: divElement
            });
        }
    },
    success: (resp, url) => {
        if (resp.status) {
            swal('Notification', 'Staff Information created successfully', 'success')
            return this.$router.push(`/home/${url}`)
        }
        return swal('Notification', 'Error Occured while creating Staff Information', 'error')
    },
    format: (amt) => {
        let fmt = null;
        let amount = amt.toString();
        if (amount.length === 4) {
            fmt = `${amount.substr(0, 1)},${amount.substr(1)}`;
        } else if (amount.length === 5) {
            fmt = `${amount.substr(0, 2)},${amount.substr(2)}`;
        } else if (amount.length === 6) {
            fmt = `${amount.substr(0, 3)},${amount.substr(3)}`;
        } else if (amount.length === 7) {
            fmt = `${amount.substr(0, 1)},${amount.substr(1, 3)},${amount.substr(3)}`;
        } else {
            fmt = amount;
        }
        return fmt;
    }
}
