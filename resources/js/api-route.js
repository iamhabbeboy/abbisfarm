export default {
    url: {
    	staff: {
	        post: '/api/staffs',
	        get: '/api/staffs',
	        put: '/api/staffs'
    	},
    	position: {
    		post: '/api/position',
	        get: '/api/position',
	        put: '/api/position'
    	},
        stipend: {
            post: '/api/stipend',
            get: '/api/stipend',
            put: '/api/stipend',
            delete: '/api/stipend'
        },
        payroll: {
            post: '/api/payroll',
            get: '/api/payroll',
            put: '/api/payroll',
            delete: '/api/payroll',
        },
        payslip: {
            post: '/api/payslip',
            get: '/api/payslip',
            put: '/api/payslip',
            delete: '/api/payslip'
        },
        salary: {
            post: '/api/salary',
            get: '/api/salary',
            put: '/api/salary',
            delete: '/api/salary'
        },
        summary: {
            post: '/api/summary',
            get: '/api/summary',
            put: '/api/summary',
            delete: '/api/summary'
        },
         users: {
            post: '/api/users',
            get: '/api/users',
            put: '/api/users',
            delete: '/api/users'
        }

    }
}
